﻿using System;

namespace SkillHomeWork2
{
    class Program
    {
        static float Russian, Story, Matem; //Переменные которые хранят оценки
        static string Ballf; //Переменная которая хранит средний балл
        static void Main(string[] args)
        {
            string Name; //Переменная которая хранит Имя
            int Age; //Переменная которая зранит Возвраст
            float Rost; //Переменная которая хранит рост
            Name = "Костя"; //Имя
            Age = 10; //Возвраст
            Rost = 1.5f; //Рост
            Matem = 5.0f; //Оценка по математике
            Russian = 5.0f; //Оценка по русскому языку
            Story = 5.0f; //Оценка по истории
            Console.BackgroundColor = ConsoleColor.DarkCyan; //Изменение цвета консоли
            Pusto(); //Вывод пустых строк в начале
            Ima(Name); //Вывод имени
            Vozrast(Age); //Вывод возвраста
            Ros(Rost); //Вывод роста
            Ball(); //Вывод среднего балла
            Console.ReadKey();

        }

        private static void Pusto()
        {
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
        }

        private static void Ros(float Rost)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"{"Рост",115}");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine($"{Rost + " m",116}");
            Console.WriteLine("");
            Console.ReadKey();
        }

        private static void Vozrast(float Age)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"{"Возраст",117}");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine($"{Age,114}");
            Console.WriteLine("");
            Console.ReadKey();
        }

        private static void Ima(string Name)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"{"Имя",115}");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine($"{Name,116}");
            Console.WriteLine("");
            Console.ReadKey();
        }

        private static void Ball()
        {
            float Ball1 = (Russian + Story + Matem) / 3;
            Ballf = Ball1.ToString("#.#");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"{"Средний", 117}");
            Console.WriteLine($"{"балл", 115}");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine($"{Ballf, 113}");
        }
    }
}
