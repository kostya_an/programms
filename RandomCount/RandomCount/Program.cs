﻿using System;

namespace RandomCount
{
    class Program
    {
        enum Process { go, noend, end }
        static Process process = Process.go;
        static Random r = new Random();
        public static int maxcount, mincount;
        static int count;
        static void Main(string[] args)
        {
            while (process == Process.go)
            {
                Console.WriteLine("Введите минимальное число");
                mincount = Convert.ToInt32(Console.ReadLine());
                Convert.ToInt64(mincount);
                Console.WriteLine("Введите максимальное число");
                maxcount = Convert.ToInt32(Console.ReadLine());
                Convert.ToInt64(maxcount);
                maxcount += 1;
                count = r.Next(mincount, maxcount);
                Console.WriteLine($"Рандомное число {count}");
                string v;
                process = Process.noend;
                while (process == Process.noend)
                {
                    Console.WriteLine("Вы хотите продолжить? (y/n)");
                    v = Console.ReadLine();
                    if (v == "y" || v == "Y" || v == "Yes" || v == "yes")
                    {
                        Console.Clear();
                        process = Process.go;
                    }
                    else if (v == "n" || v == "N" || v == "No" || v == "no")
                    {
                        Console.Clear();
                        process = Process.end;
                    }
                    else
                    {
                        Console.Clear();
                        process = Process.noend;
                    }
                }
                if (process == Process.end)
                {
                    Console.WriteLine("До свидания!");
                    Console.ReadKey();
                }

            }
        }
    }
}
