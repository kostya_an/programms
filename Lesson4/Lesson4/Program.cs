﻿using System;

namespace Lesson4
{
    class Program
    {
        class Car
        {
            private string bag = "Паспорт";
            public string color;
            public string mark;
            public int maxspeed;


            public Car(string c, string m, int mxs)
            {
                color = c;
                mark = m;
                maxspeed = mxs;
            }
            public Car()
            {

                mark = "LADA";
                color = "white";
                maxspeed = 80;
            }

            public void Log()
            {
                Lookbag();
                Console.WriteLine($"Цвет: {color}, Марка: {mark}, Максимальная скорость: {maxspeed}");
            }
            public void Update()
            {
                int Delt = 5;
                maxspeed = maxspeed + Delt;
            }
            protected void Lookbag()
            {
                Console.WriteLine(bag);
            }

        }
        
        class Bus:Car
        {

            public int Num;
            public Bus(int N, string c, string m, int mxs)
                : base(c, m, mxs)
            {
                Num = N;
            }
            public void Log()
            {
                Lookbag();
                Console.WriteLine($"Макс. кол-во пассажиров: {Num}, Цвет: {color}, Марка: {mark}, Максимальная скорость: {maxspeed}");
            }
        }

        static void Main(string[] args)
        {
            Car car0 = new Car("black", "LADA", 80);
            Car car1 = new Car("white", "Ford", 100);
            Car car2 = new Car();
            Bus bus0 = new Bus(20, "Blue", "Mersedes-benz", 70);

            //string color1 = "green";
            //string mark1 = "LADA";
            //int maxspeed1 = 80;
            //Console.WriteLine($"Цвет: {car0.color}, Марка: {car0.mark}");
            //Console.WriteLine($"Цвет: {car1.color}, Марка: {car1.mark}");
            //Console.WriteLine($"Цвет: {car2.color}, Марка: {car2.mark}");
            
            car0.Log();
            car1.Log();
            car2.Log();
            bus0.Log();

            car0.Update();
            car0.Log();
            //bus0.Lookbag();
            //Console.WriteLine(bus0.bag);
        }
    }
}
