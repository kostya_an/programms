﻿using System;

namespace Spec
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите число n");
            int n = Convert.ToInt32(Console.ReadLine());
            int counter = 0;
            //int countSteps = 0;
            for (int i = n; i > 0; i--)
            {
                Console.WriteLine($"{counter} + {i} = {counter+i}");
                counter += i;
                
                //countSteps++;
            }
            Console.WriteLine(counter);
            //Console.WriteLine(countSteps + " - кол-во всех операций");
        }
    }
}
