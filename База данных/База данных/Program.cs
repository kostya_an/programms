﻿using System;

namespace База_данных
{
    class Program
    {
        static void Main(string[] args)
        {
            #region База данных 2
            string Name = "Имя"; string LastName = "Фамилия"; string Age = "Возвраст"; string yearOfBirth = "Год рождения"; string salary = "Зарплата";
            string Name1 = "Агата"; string LastName1 = "Агапова"; string Age1 = "35"; string yearOfBirth1 = "1985"; float salary1 = 25000;
            string Name2 = "Анна"; string LastName2 = "Анисимова"; string Age2 = "25"; string yearOfBirth2 = "1995"; float salary2 = 35000;
            string Name3 = "Елена"; string LastName3 = "Лебедева"; string Age3 = "30"; string yearOfBirth3 = "1990"; float salary3 = 30000;
            string Name4 = "Елизавета"; string LastName4 = "Еленова"; string Age4 = "45"; string yearOfBirth4 = "1975"; float salary4 = 20000;
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine($"{Name,10} {LastName,12} {Age,10} {yearOfBirth,14} {salary,10}");
            Console.WriteLine("                                             ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"{Name1,10} {LastName1,12} {Age1,10} {yearOfBirth1,14} {salary1,10}");
            Console.WriteLine($"{Name2,10} {LastName2,12} {Age2,10} {yearOfBirth2,14} {salary2,10}");
            Console.WriteLine($"{Name3,10} {LastName3,12} {Age3,10} {yearOfBirth3,14} {salary3,10}");
            Console.WriteLine($"{Name4,10} {LastName4,12} {Age4,10} {yearOfBirth4,14} {salary4,10}");
            Console.ReadKey();
            #endregion
        }
    }
}
