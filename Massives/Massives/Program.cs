﻿using System;

namespace Massives
{
    class Program
    {
        public static int[] array = new int[100];
        public static int[] array2 = new int[1000];
        public static char[] lot = new char[33];

        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            int hundreds = 0;
            int count;
            Random r = new Random();
            Console.WriteLine("Введите число кол-во которого нужно вывести");
            count = Convert.ToInt32(Console.ReadLine());
            for(int i = 0; i < array.Length; i++)
            {
                array.SetValue(r.Next(1, 101), i);
            }
            for (int i = 0; i < array2.Length; i++)
            {
                array2.SetValue(r.Next(101, 1001), i);
                if (array2[i] == count)
                {
                    hundreds++;
                }
            }
            //Array.Copy(array, array2, array.Length);
            Array.Sort(array2);
            

            foreach (int i in array2)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine($"Вывелось чисел {count} : {hundreds}");
        }
    }
}
