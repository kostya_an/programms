﻿using System;

namespace Apples_and_Puples
{
    class Program
    {
        static int apples;
        static int pupils;
        static void Main(string[] args)
        {
            try
            {
                Start();

            }  catch (FormatException)
            {
                Console.WriteLine("Вы написали не число");
            }
            try
            {
                Console.WriteLine($"Каждый ученик получил {apples / pupils} яблок(а, о)");
                Console.WriteLine($"В корзине осталось {apples % pupils} яблок(а, о)");
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Делить на 0 нельзя");
                Start();
            }
            
        }
        static void Start()
        {
            Console.WriteLine("Введите кол-во яблок!");
            apples = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите кол-во учеников");
            pupils = Convert.ToInt32(Console.ReadLine());
        }
    }
}
