﻿using System;

namespace EventTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player1 = new Player(0, "Michael");
            Player player2 = new Player(1, "Jack");
            UI ui = new UI();
            Console.WriteLine("Hello World!");
            player1.Goal();
            player2.Goal();
        }
    }
    class Player
    {
        public static event Action<Player> OnGoal;
        #region Свойства
        int id;
        string name;
        #endregion
        public string Name { get { return name; } }
        public Player(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
        public void Goal()
        {
            OnGoal.Invoke(this);
        }
    }
    class UI
    {
        public UI()
        {
            Player.OnGoal += Print;
        }
        ~UI()
        {
            Player.OnGoal -= Print;
        }
        void Print(Player player)
        {
            Console.WriteLine("Gooool!!");
            Console.WriteLine($"{player.Name} WINS!");
        }
    }
}
