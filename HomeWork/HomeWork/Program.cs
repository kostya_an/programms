﻿using System;

namespace HomeWork
{
    class Program
    {
        public enum Process { go, noend, end};
        public static Process process = Process.go;
        public static int day = 0;
        public static string v;
        public static string matem = "Математика";
        public static string russian = "Русский язык";
        public static string lit = "Литературное чтение";
        public static string okr = "Оружающий мир";
        public static string eng = "Английский язык";
        public static string fiz = "Физ-ра";

        static void Main()
        {
            while(process == Process.go)
            {
                Console.WriteLine("Введите день недели");
                day = Convert.ToInt32(Console.ReadLine());
                if(day == 1)
                {
                    DayWork("Понедельник", fiz, eng, matem, russian, "Внеурочная деятельность (математика)");
                }
                if (day == 2)
                {
                    DayWork("Вторник", matem, "Родной язык", lit, okr, "Основы мировых религиозных культур");
                }
                if (day == 3)
                {
                    DayWork("Среда", russian, "Работа с текстом (русский язык)", fiz, eng, matem);
                }
                if (day == 4)
                {
                    DayWork("Четверг", russian, matem, "Музыка", lit, okr);
                }
                if (day == 5)
                {
                    DayWork("Пятница", lit, matem, russian, "Технология", "Внеурочная деятельность (окружающий мир)");
                }
                if(day == 6)
                {
                    Console.WriteLine("В субботу нет уроков");
                }
                if (day == 7)
                {
                    Console.WriteLine("В воскресенье нет уроков");
                }
                if (day >= 8)
                {
                    Console.WriteLine("В неделе 7 дней");
                }
                while (process == Process.noend)
                {
                    Console.WriteLine("Вы хотите продолжить? (y/n)");
                    v = Console.ReadLine();
                    if (v == "y" || v == "Y" || v == "Yes" || v == "yes")
                    {
                        process = Process.go;
                        Console.Clear();
                    }
                    else if (v == "n" || v == "N" || v == "No" || v == "no")
                    {
                        process = Process.end;
                    }
                    else
                    {
                        process = Process.noend;
                    }
                }
            }
            if(process == Process.end)
            {
                Console.WriteLine("До свидания!");
                Console.ReadKey();
            }
        }
        public static void DayWork(string NameDay, string one, string two, string three, string four, string five)
        {
            Console.WriteLine("");
            Console.WriteLine(NameDay);
            Console.WriteLine("");
            Console.WriteLine(one);
            Console.WriteLine(two);
            Console.WriteLine(three);
            Console.WriteLine(four);
            Console.WriteLine(five);
            Console.WriteLine("");
            process = Process.noend;
        }
    }
}
