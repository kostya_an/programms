﻿using System;

namespace Calculator
{
    class Program
    {

            int i1;
            int i2;
            char c;
            string i1s;
            string i2s;
            string cs;
        
        public void Input()
        {
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Введите первое число");
            i1s = Console.ReadLine();
            Console.WriteLine("Введите второе число");
            i2s = Console.ReadLine();
            Console.WriteLine("Введите математический знак + - * / ^ %");
            cs = Console.ReadLine();
            i1 = Convert.ToInt32(i1s);
            i2 = Convert.ToInt32(i2s);
            c = Convert.ToChar(cs);

        }
        public void Calc()
        {
            int b;
            if (c == '+')
            {
                b = i1 + i2;
                Console.WriteLine($"Первое число сложенное со вторым числом = {b}");
            }
            else if(c == '-')
            {
                b = i1 - i2;
                Console.WriteLine($"Первое число вычтено второе число{b}");
            }
            else if (c == '*')
            {
                b = i1 * i2;
                Console.WriteLine($"Первое число умноженное на второе число = {b}");
            }
            else if (c == '/')
            {
                if(i2 == 0)
                {
                    Console.WriteLine("Делить на 0 нельзя");
                    return;
                }

                b = i1 / i2;
                Console.WriteLine($"Первое число делённое на второе число = {b}");
            }
            else if (c == '^')
            {
                int result = 1;
                for(int i = 0; i < i2; i++)
                {
                    result *= i1;
                }
                
                Console.WriteLine($"Первое число в степени второго числа = {result}");
            }
            else if (c == '%')
            {
                if (i2 == 0)
                {
                    Console.WriteLine("Делить на 0 нельзя");
                    return;
                }
                b = i1 % i2;
                Console.WriteLine($"Первое число делённое без остатка на второе число = {b}");
            }
            else
            {
                return;
            }
        }
             
    }
}
