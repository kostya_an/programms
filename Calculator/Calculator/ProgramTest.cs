﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    class ProgramTest
    {
        enum Pr { go, noend, end }
        static Pr Prog = Pr.go;
        static void Main(string[] args)
        {
            string v;
            Program program = new Program();
            while(Prog == Pr.go)
            {
                program.Input();

                program.Calc();

                Prog = Pr.noend;

                while (Prog == Pr.noend)
                {
                    Console.WriteLine("Вы хотите продолжить? (y/n)");
                    v = Console.ReadLine();
                    if (v == "y" || v == "Y" || v == "Yes" || v == "yes")
                    {
                        Prog = Pr.go;
                    }
                    else if (v == "n" || v == "N" || v == "No" || v == "no")
                    {
                        Prog = Pr.end;
                    }
                    else
                    {
                        Prog = Pr.noend;
                    }
                }
                if(Prog == Pr.end)
                {
                    Console.WriteLine("До свидания!");
                    Console.ReadKey();
                }
                
            }
            
        }
    }
}
