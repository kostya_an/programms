﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Дежурства
{
    class Program
    {
        static StreamWriter sw = new StreamWriter("C:/Users/kpani/source/repos/Дежурства/Дежурства/Latest.txt", true, Encoding.UTF8);

        static void Main(string[] args)
        {
            
            
            Console.WriteLine("Введите месяц");
            string month = Console.ReadLine();
            Console.WriteLine("Введите номер группы которая должна быть первой в этом месяце\n(номер завершающей группы пред. месяца + 1)");
            int group = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите нерабочие дни через пробел");
            string[] noWorkDaysString = Console.ReadLine().Split(" ");
            List<int> noWorkDays = new List<int>();
            
            
            
            foreach(string i in noWorkDaysString)
            {
                noWorkDays.Add(Convert.ToInt32(i));
            }
            Dictionary<string,int> months = new Dictionary<string,int>()
            {
                {"Январь", 1},
                {"Февраль", 2 },
                {"Март", 3 },
                {"Апрель", 4 },
                {"Май", 5 },
                {"Июнь", 6 },
                {"Июль", 7 },
                {"Август", 8 },
                {"Сентябрь", 9 },
                {"Октябрь", 10 },
                {"Ноябрь", 11 },
                {"Декабрь", 12 }
            };
            int daysofmonth = DateTime.DaysInMonth(2022, months[month]);
            Console.WriteLine();
            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine("Номер месяца: " + months[month].ToString());
            Console.WriteLine("Дней в месяце: " + daysofmonth);
            Console.WriteLine("Нерабочие дни:");
            foreach(int i in noWorkDays)
            {
                Console.Write(i + ", ");
            }
            Console.WriteLine();
            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine();
            Dictionary<int, List<string>> pairs = new Dictionary<int, List<string>>()
            {
                {1, new List<string>{ "Анисимов", "Киселёва", "Пустарнакова" }},
                {2, new List<string>{ "Ахапкина", "Коркин", "Рябченко" }},
                {3, new List<string>{ "Беляков", "Коростылёв", "Сверчков" }},
                {4, new List<string>{ "Бурашникова", "Кузьмина", "Свинчук" }},
                {5, new List<string>{ "Винокур", "Куроедова", "Сергеева" }},
                {6, new List<string>{ "Дубровский", "Лаптева", "Скориков" }},
                {7, new List<string>{ "Зияудинова", "Лефёрова", "Сорокин" }},
                {8, new List<string>{ "Игашёва", "Либеранский", "Сосковец" }},
                {9, new List<string>{ "Исайчев", "Маилян", "Трунов" }},
                {10, new List<string>{ "Калашников", "Мишаева", "Харламов" }},
                {11, new List<string>{ "Ким", "Новосельский", "Чебуркоаа" }}
            };
            //pairs.Add(0, new List<string> { "Анисимов", "Дубровский", "Ким" });
            foreach(var i in PairsToDays(daysofmonth, pairs, months[month], group, noWorkDays))
            {
                Console.WriteLine(i.Key);
                foreach(var j in i.Value)
                {
                    Console.WriteLine(j);
                    
                }
                Console.WriteLine();
                
            }
            
            sw.Write(months[month]);
           
            sw.Close();
        }
        static Dictionary<int, List<string>> PairsToDays(int days, Dictionary<int, List<string>> pairs, int month, int group, List<int> noWork)
        {
            Dictionary<int, List<string>> groups = new Dictionary<int, List<string>>();
            int j = group;
            for (int i = 1; i <= days; i++)
            {
                if(j >= pairs.Count)
                {
                    j = 1;
                }
                DateTime someDate = new DateTime(2022, month, i);
                if(someDate.DayOfWeek.ToString() == "Saturday")
                {
                    i += 2;
                }
                if(noWork.Contains(i))
                {
                    continue;
                }
                groups.Add(i, pairs[j]);
                j++;
            }
            sw.Write(j + " ");
            return groups;
        }
    }
}