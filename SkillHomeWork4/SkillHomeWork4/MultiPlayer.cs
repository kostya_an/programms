﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SkillHomeWork4
{
    class MultiPlayer
    {
        public static int player = 0;
        public static Random r = new Random();
        public static int Count;
        public static int PlayerCount;
        public static int CountNames;
        public static int MaxPlayerCount;
        
        
        public static void Play()
        {
            List<string> Names = new List<string>();
            Console.WriteLine("Введите кол-во человек");
            CountNames = Convert.ToInt32(Console.ReadLine());
            while(CountNames <= 1)
            {
                Console.WriteLine("В режиме нескольких игроков не может быть один игрок");
                CountNames = Convert.ToInt32(Console.ReadLine());
            }
            for (int i = 1; i <= CountNames; i++)
            {
                Console.WriteLine($"Введите имя {i} игрока");
                Names.Add(Console.ReadLine());
            }
            Count = r.Next(12, 121);
            if (CountNames % 5 == 0 && CountNames > 5)
            {
                Count += 100;
            }

            while (Program.process == Program.Process.go)
            {
                if (CountNames <= player)
                {
                    player = 0;
                }
                Console.WriteLine($"Число {Count}");
                Console.WriteLine($"{Names[player]} ходит");
                PlayerCount = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("");

                while (PlayerCount > MaxPlayerCount)
                {
                    Console.WriteLine($"Вы не можете писать числа больше {MaxPlayerCount}");
                    Console.WriteLine("");
                    Console.WriteLine($"Число {Count}");
                    Console.WriteLine($"{Names[player]} ходит");
                    PlayerCount = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("");
                }
                while (PlayerCount == 0)
                {
                    Console.WriteLine($"Вы не можете ходить нулём");
                    Console.WriteLine($"Число {Count}");
                    Console.WriteLine($"{Names[player]} ходит");
                    PlayerCount = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("");
                }
                Count -= PlayerCount;
                if(Count <= 0)
                {
                    Console.WriteLine($"Победил(-a) {Names[player]}");
                    Program.process = Program.Process.noend;
                }
                player++;
            } 
        }
    }
}
