﻿using System;
using System.Collections.Generic;

namespace SkillHomeWork4
{
    class Program
    {
        public enum Process {go, noend, end };
        public static Process process = Process.go;
        static void Main(string[] args)
        {
            string v;
            int CountPlayers;
            int DifficultyLevel;
            bool SelectDifficulty = false;
            bool SelectMode = false;
            Console.WriteLine("Цель игры: Игра загадывает число и два игрока по очереди вычитают числа\nи кто первый доведёт число до нуля тот и победил");
            Console.WriteLine("");
            while(process == Process.go)
            {
                while (!SelectDifficulty)
                {
                    Console.WriteLine("Введите уровень сложности");
                    Console.WriteLine("Простой - 1, средний - 2, сложный - 3");
                    DifficultyLevel = Convert.ToInt32(Console.ReadLine());
                    if (DifficultyLevel == 1)
                    {
                        Console.WriteLine("В простом уровне сложности вы не можете ходить числами больше 4");
                        Console.WriteLine("");
                        MultiPlayer.MaxPlayerCount = 4;
                        OnePlayer.MaxPlayerCount = 4;

                        SelectDifficulty = true;
                    }
                    else if (DifficultyLevel == 2)
                    {
                        Console.WriteLine("В среднем уровне сложности вы не можете ходить числами больше 6");
                        Console.WriteLine("");
                        MultiPlayer.MaxPlayerCount = 6;
                        OnePlayer.MaxPlayerCount = 6;
                        SelectDifficulty = true;
                    }
                    else if (DifficultyLevel == 3)
                    {
                        Console.WriteLine("В сложном уровне сложности вы не можете ходить числами больше 8");
                        Console.WriteLine("");
                        MultiPlayer.MaxPlayerCount = 8;
                        OnePlayer.MaxPlayerCount = 8;
                        SelectDifficulty = true;
                    }
                    else
                    {
                        SelectDifficulty = false;
                    }
                }
                while (!SelectMode)
                {
                    Console.WriteLine("Выберете режим");
                    Console.WriteLine("1 - игра с ботом, 2 - игра с людьми");
                    CountPlayers = Convert.ToInt32(Console.ReadLine());
                    if (CountPlayers == 1)
                    {
                        OnePlayer.Play();
                        SelectMode = true;
                    }
                    else if (CountPlayers == 2)
                    {
                        MultiPlayer.Play();
                        SelectMode = true;
                    }
                    else
                    {
                        SelectMode = false;
                    }
                }



                while (process == Process.noend)
                {
                    Console.WriteLine("Вы хотите продолжить? (y/n)");
                    v = Console.ReadLine();
                    if (v == "y" || v == "Y" || v == "Yes" || v == "yes")
                    {
                        process = Process.go;
                        Console.Clear();
                        SelectDifficulty = false;
                        SelectMode = false;
                    }
                    else if (v == "n" || v == "N" || v == "No" || v == "no")
                    {
                        process = Process.end;
                    }
                    else
                    {
                        process = Process.noend;
                    }
                }
                if (process == Process.end)
                {
                    Console.WriteLine("До свидания!");
                    Console.ReadKey();
                }
            }
        }
    }
}
