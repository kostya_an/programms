﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SkillHomeWork4
{
    class OnePlayer
    {
        public static Random r = new Random();
        public static int Count;
        public static int PlayerCount;
        public static int MaxPlayerCount;
        public static string Name;
        public static int BotCount;
        public static string BotName;
        public static void Play()
        {
            Console.WriteLine("Введите ваше имя");
            Name = Console.ReadLine();
            Console.WriteLine("Введите имя бота");
            BotName = Console.ReadLine();
            Count = r.Next(12, 120);
            while (Program.process == Program.Process.go)
            {
                Console.WriteLine($"Число {Count}");
                Console.WriteLine($"{Name} ходит");
                PlayerCount = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("");
                while (PlayerCount > MaxPlayerCount)
                {
                    Console.WriteLine($"Вы не можете писать числа больше {MaxPlayerCount}");
                    Console.WriteLine($"Число {Count}");
                    Console.WriteLine($"{Name} ходит");
                    PlayerCount = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("");
                }
                while (PlayerCount == 0)
                {
                    Console.WriteLine($"Вы не можете ходить нулём");
                    Console.WriteLine($"Число {Count}");
                    Console.WriteLine($"{Name} ходит");
                    PlayerCount = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("");
                }
                Count -= PlayerCount;
                if (Count <= 0)
                {
                    Console.WriteLine($"Победил(-a) {Name}");
                    Program.process = Program.Process.noend;
                    break;
                }
                
                if (Count > MaxPlayerCount)
                {
                    Console.WriteLine($"Число {Count}");
                    Console.WriteLine($"{BotName} ходит");
                    BotCount = r.Next(MaxPlayerCount / 2, MaxPlayerCount);
                    Console.WriteLine(BotCount);
                    Console.WriteLine("");
                    
                    Count -= BotCount;
                    if(Count <= 0)
                    {
                        Console.WriteLine($"Победил(-а) {BotName}");
                        Program.process = Program.Process.noend;
                    }
                }
                else if (Count == MaxPlayerCount)
                {
                    Console.WriteLine($"Число {Count}");
                    Console.WriteLine($"{BotName} ходит");
                    BotCount = MaxPlayerCount;
                    Console.WriteLine(BotCount);
                    Console.WriteLine("");

                    Count -= BotCount;
                    if (Count <= 0)
                    {
                        Console.WriteLine($"Победил(-а) {BotName}");
                        Program.process = Program.Process.noend;
                    }
                }
                else if (Count < MaxPlayerCount)
                {
                    Console.WriteLine($"Число {Count}");
                    Console.WriteLine($"{BotName} ходит");
                    Console.WriteLine(Count);
                    Console.WriteLine("");
                    BotCount = Count;
                    Count -= BotCount;
                    if (Count <= 0)
                    {
                        Console.WriteLine($"Победил(-а) {BotName}");
                        Program.process = Program.Process.noend;
                    }
                }
                
            }
        }
    }
}
