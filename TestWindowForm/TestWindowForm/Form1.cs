﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestWindowForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hello World");
            button1.FlatStyle = FlatStyle.Popup;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            button1_Click(sender, e);
        }
    }
}
