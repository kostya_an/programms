﻿using System;
using System.Collections.Generic;

namespace Fortuna
{
    class Program
    {
        public static List<string> Random = new List<string>();
        
        static void Main(string[] args)
        {
            bool Variants = true;
            int CountElements = 1;
            string b;
            System.Random r = new Random();
            while (Variants)
            {
                Console.WriteLine($"Введите {CountElements} элемент");
                Console.WriteLine("Введите +- для продолжения");
                b = Console.ReadLine();
                if(b == "+-")
                {
                    Variants = false;
                }
                Random.Add(b);
                CountElements++;
            }
            int random = r.Next(0, Random.Count + 1);
            Console.WriteLine("");
            Console.WriteLine($"Ответ: {Random[random]}");
        }
    }
}
