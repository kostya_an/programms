﻿using System;

namespace Lesson3
{
    class Program
    {
        enum herotype { archer, warrior, wizard}

        static void Main(string[] args)
        {
            int x = 5;
            Double(x);
            Double(10);
            
            int y = Double(10);
            Console.WriteLine(y);

            int c = Test(11);
            Console.WriteLine(c);
            herotype hero = herotype.archer;
            if(hero == herotype.archer)
            {
                Console.WriteLine("bow");
            }
            Console.WriteLine(hero);
            Console.ReadKey();
            
        }

        private static int Double(int n)
        {
            //int Count = Console.WriteLine();
            //Console.WriteLine(n);
            return n*2;
        }

        private static int Test(int d)
        {
            if(d % 2 == 0)
            {
                return d * 2;
            }

            else
            {
                return d / 2;
            }
        }

    }
}
