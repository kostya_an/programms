﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Numbers
{
    class One
    {
        enum Procces { go, end }
        static Procces ProccesGame;
        public static void Game()
        {
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("Введите ваше имя");

            Console.ForegroundColor = ConsoleColor.Blue;
            string Name = Console.ReadLine();
            Console.Clear();
            Random r = new Random();
            int Count = r.Next(1, 101);
            ProccesGame = Procces.go;
            int Countuser;
            string Countuserstring;
            int Raz = 0;
            while(ProccesGame == Procces.go)
            {
                Raz = Raz + 1;
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"Ваша {Raz} попытка");
                Console.ForegroundColor = ConsoleColor.Blue;
                Countuserstring = Console.ReadLine();
                Console.Clear();
                Countuser = Convert.ToInt32(Countuserstring);

                if(Countuser == Count)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"{Name}, вы победили за {Raz} попытк(-у,-и,-ок)");
                    Console.WriteLine("");
                    Console.ReadKey();
                    Console.Clear();
                    ProccesGame = Procces.end; 
                }
                else if(Countuser < Count)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ваше число меньше загаданого");
                    Console.WriteLine("");
                    Console.ReadKey();
                    Console.Clear();

                }
                else if(Countuser > Count)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ваше число больше загаданого");
                    Console.WriteLine("");
                    Console.ReadKey();
                    Console.Clear();
                }
            }
        }
    }
}
