﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Numbers
{
    
    class Two
    {
        enum Procces { go, end }
        static Procces ProccesGame;
        public static void Game()
        {
            string Name1;
            string Name2;
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("Введите имя 1 игрока");
            Console.ForegroundColor = ConsoleColor.Blue;
            Name1 = Console.ReadLine();
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("Введите имя 2 игрока");
            Console.ForegroundColor = ConsoleColor.Blue;
            Name2 = Console.ReadLine();
            Console.Clear();
            Random r = new Random();
            int Count;
            Count = r.Next(1, 101);
            ProccesGame = Procces.go;
            int Raz1 = 0;
            int Raz2 = 0;
            int Countuser1;
            int Countuser2;

            string Countuserstring1;
            string Countuserstring2;
            while(ProccesGame == Procces.go)
            {
                #region 1 игрок
                Raz1++;
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"{Raz1} попытка игрока {Name1}");
                Console.ForegroundColor = ConsoleColor.Blue;
                Countuserstring1 = Console.ReadLine();

                Console.Clear();
                Countuser1 = Convert.ToInt32(Countuserstring1);
                if (Countuser1 == Count)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"{Name1}, вы победили за {Raz1} попытк(-у,-и,-ок)");
                    Console.WriteLine("");
                    Console.ReadKey();
                    Console.Clear();
                    ProccesGame = Procces.end;
                    break;
                }
                else if (Countuser1 < Count)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ваше число меньше загаданого");
                    Console.WriteLine("");
                    Console.ReadKey();
                    Console.Clear();
                }
                else if (Countuser1 > Count)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ваше число больше загаданого");
                    Console.WriteLine("");
                    Console.ReadKey();
                    Console.Clear();
                }
                #endregion

                #region 2 игрок
                Raz2 = Raz2 + 1;
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"{Raz2} попытка игрока {Name2}");
                Console.ForegroundColor = ConsoleColor.Blue;
                Countuserstring2 = Console.ReadLine();
                Console.Clear();
                Countuser2 = Convert.ToInt32(Countuserstring2);
                if (Countuser2 == Count)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"{Name2}, вы победили за {Raz2} попытк(-у,-и,-ок)");
                    Console.WriteLine("");
                    Console.ReadKey();
                    Console.Clear();
                    ProccesGame = Procces.end;
                    break;
                }
                else if (Countuser2 < Count)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ваше число меньше загаданого");
                    Console.WriteLine("");
                    Console.ReadKey();
                    Console.Clear();
                }
                else if (Countuser2 > Count)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ваше число больше загаданого");
                    Console.WriteLine("");
                    Console.ReadKey();
                    Console.Clear();
                }
                #endregion
            }
        }
    }
}
