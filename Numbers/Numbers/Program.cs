﻿using System;

namespace Numbers
{
    class Program
    {
        enum Procces { go, noend, end}
        
        static Procces procces;

        
        static void Main(string[] args)
        {
            
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Цель игры: Игра загадывает число в диапозоне от 1 до 100, а вы должны его отгадать. Есть одиночный и двойной режим игры.\nВ одиночном нужно просто отгадать число. А во втором побеждает тот кто первый отгадал");

            Console.ReadKey();
            Console.Clear();
            string Rejim;
            string v;
            while (procces == Procces.go)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Введите название режима: одиночный или двойной");
                Console.ForegroundColor = ConsoleColor.White;
                Rejim = Console.ReadLine();
                Console.Clear();
                if (Rejim == "1" || Rejim == "Одиночный" || Rejim == "одиночный" || Rejim == "Один" || Rejim == "один")
                {
                    One.Game();
                }
                else if (Rejim == "2" || Rejim == "Двойной" || Rejim == "двойной" || Rejim == "Два" || Rejim == "два")
                {
                    Two.Game();
                }
                else
                {
                    
                }
                procces = Procces.noend;
                while (procces == Procces.noend)
                {
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("Вы хотите продолжить? (y/n)");
                    Console.ForegroundColor = ConsoleColor.White;
                    v = Console.ReadLine();
                    Console.Clear();
                    if (v == "y" || v == "Y" || v == "Yes" || v == "yes")
                    {
                        procces = Procces.go;
                    }
                    else if (v == "n" || v == "N" || v == "No" || v == "no")
                    {
                        procces = Procces.end;
                    }
                    else
                    {
                        procces = Procces.noend;
                    }
                }
                if (procces == Procces.end)
                {
                    Console.WriteLine("До свидания!");
                    Console.ReadKey();
                    Console.Clear();
                }
            }
            
        }
    }
}
