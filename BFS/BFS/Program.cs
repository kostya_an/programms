﻿using System;
using System.Collections.Generic;

namespace BFS
{

    class Position : IEquatable<Position>
    {
        public List<Tuple<int, int>> path = new List<Tuple<int, int>>();
        public int y;
        public int x;
        public Position(int y, int x, List<Tuple<int, int>> p, Tuple<int,int> cur)
        {
            this.y = y;
            this.x = x;
            for(int i = 0; i < p.Count; i++)
            {
                path.Add(p[i]);
            }
            path.Add(cur);
        }
        public Position(int y, int x)
        {
            this.y = y;
            this.x = x;
        }

        public bool Equals(Position other)
        {
            //throw new NotImplementedException();
            return this.x == other.x && this.y == other.y;
        }
    }
    class Program
    {
        public static string[] maze =
           {
            "#######################",
            "#   ##        ####  A##",
            "#   #######   ####   ##",
            "#             ####   ##",
            "#   #######          ##",
            "## ###########   ######",
            "## ###########   ######",
            "##        ####  B######",
            "#######################"
            };
        static void Main(string[] args)
        {
            bool changed = false;
            while (!changed)
            {
                Console.Clear();
                Console.WriteLine("Введите, каким методом вы хотите найти путь");
                Console.WriteLine("Варианты: BFS - поиск в ширину \n          DFS - поиск в глубину");
                string value = Console.ReadLine();
                if(value != "BFS" && value != "DFS")
                {
                    Console.WriteLine("Вы ввели неккоректное значение");
                    Console.ReadKey();
                }
                if(value == "BFS")
                {
                    Console.Clear();
                    BFS bfs = new BFS();
                    bfs.Do();
                    //changed = true;

                }
                if(value == "DFS")
                {
                    Console.Clear();
                    DFS dfs = new DFS(maze);
                    dfs.Do();
                    //changed = true;
                }
            }
            
        }
    }
    class BFS
    {
        public static string[] maze =
            Program.maze;
        public static bool[,] marked = new bool[maze.Length, maze[0].Length];
        public static bool[,] visited = new bool[maze.Length, maze[0].Length];

        public void Do()
        {
            Queue<Position> positions = new Queue<Position>();
            Position curPos = StartPosition();
            
            marked[curPos.y, curPos.x] = true;
            int countStrokes = 0;
            //Console.WriteLine(maze[1][2]);
            //Console.WriteLine(maze[curPos.y][curPos.x]);
            positions.Enqueue(curPos);
            while(maze[curPos.y][curPos.x] != 'B')
            {
                //if (!visited[curPos.y, curPos.x])
                //{
                curPos = positions.Dequeue();
                visited[curPos.y, curPos.x] = true;
                countStrokes++;
                //}
                //else
                //{
                //    positions.Dequeue();
                //}
                Print(curPos);
                if (curPos.y + 1 < maze.Length && maze[curPos.y + 1][curPos.x] != '#' && !marked[curPos.y + 1, curPos.x])   //Проверка для перехода вверх
                {
                    positions.Enqueue(new Position(curPos.y + 1, curPos.x, curPos.path, Tuple.Create(curPos.y + 1, curPos.x)));
                    marked[curPos.y + 1, curPos.x] = true;
                }
                if(curPos.x + 1 < maze[0].Length && maze[curPos.y][curPos.x + 1] != '#' && !marked[curPos.y, curPos.x + 1])   //Проверка для перехода вправо
                {
                    positions.Enqueue(new Position(curPos.y, curPos.x + 1, curPos.path, Tuple.Create(curPos.y, curPos.x + 1)));
                    marked[curPos.y, curPos.x + 1] = true;

                }
                if (curPos.y - 1 >= 0 && maze[curPos.y - 1][curPos.x] != '#' && !marked[curPos.y - 1, curPos.x])   //Проверка для перехода вниз
                {
                    positions.Enqueue(new Position(curPos.y - 1, curPos.x, curPos.path, Tuple.Create(curPos.y - 1, curPos.x)));
                    marked[curPos.y - 1, curPos.x] = true;
                }
                if (curPos.x - 1 >= 0 && maze[curPos.y][curPos.x - 1] != '#' && !marked[curPos.y, curPos.x - 1])   //Проверка для перехода влево
                {
                    positions.Enqueue(new Position(curPos.y, curPos.x - 1, curPos.path, Tuple.Create(curPos.y, curPos.x - 1)));
                    marked[curPos.y, curPos.x - 1] = true;
                }
                Console.ReadKey();
                
            }
            if(maze[curPos.y][curPos.x] == 'B')
            {
                Console.WriteLine();
                Console.WriteLine("Найден B");
                Console.WriteLine();
                Console.WriteLine($"y = {curPos.y}");
                Console.WriteLine($"x = {curPos.x}");
                Console.WriteLine();
                Console.WriteLine($"Количество ходов: {countStrokes}");
                PathPrint(curPos.path);
                for(int i = 0; i < maze.Length; i++)
                {
                    for(int j = 0; j < maze[0].Length; j++)
                    {
                        marked[i, j] = false;
                    }
                }
            }
            Console.ReadKey();
        }

        static Position StartPosition()
        {
            for(int i = 0;i < maze.Length; i++)
            {
                for(int j = 0;j < maze[i].Length; j++)
                {
                    if(maze[i][j] == 'A')
                    {
                        return new Position(i, j);
                    }
                }
            }
            throw new Exception("Start position not found.");   
        }
        static void Print(Position pos)
        {
            for (int i = 0; i < maze.Length; i++)
            {
                for (int j = 0; j < maze[i].Length; j++)
                {
                    if (i == pos.y && j == pos.x)
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write("*");
                        Console.ForegroundColor = ConsoleColor.White;

                    }
                    else if (maze[i][j] == 'A')
                    {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.Write("A");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (maze[i][j] == 'B')
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("B");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (visited[i, j] == true)
                    {
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.Write(".");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (maze[i][j] == '#')
                    {
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.Write("#");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.Write(maze[i][j]);
                    }

                }
                Console.WriteLine();
            }
            Console.WriteLine($"y = {pos.y}");
            Console.WriteLine($"x = {pos.x}");
        }
        static void PathPrint(List<Tuple<int, int>> path)
        {
            Console.WriteLine("Путь:");
            Console.WriteLine();
            for (int i = 0; i < maze.Length; i++)
            {
                for (int j = 0; j < maze[i].Length; j++)
                {
                    bool contains = false;
                    foreach(var p in path)
                    {
                        if(i == p.Item1 && j == p.Item2)
                        {
                            contains = true;
                            break;
                        }
                    }
                    if (maze[i][j] == 'A')
                    {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.Write("A");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (maze[i][j] == 'B')
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("B");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (contains)
                    {
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.Write(".");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (maze[i][j] == '#')
                    {
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.Write("#");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.Write(maze[i][j]);
                    }

                }
                Console.WriteLine();
            }
        }
    }
    class DFS
    {
        /*public */string[] maze;
        /*public */bool[,] marked;
        /*public */bool[,] visited;
        public DFS(string[] maze)
        {
            this.maze = maze;
            marked = new bool[maze.Length, maze[0].Length];
            visited = new bool[maze.Length, maze[0].Length];
        }
        public void Do()
        {
            Stack<Position> positions = new Stack<Position>();
            Position curPos = StartPosition();
            marked[curPos.y, curPos.x] = true;
            int countStrokes = 0;
            //Console.WriteLine(maze[1][2]);
            //Console.WriteLine(maze[curPos.y][curPos.x]);
            positions.Push(curPos);
            while (maze[curPos.y][curPos.x] != 'B')
            {
                //if (!visited[curPos.y, curPos.x])
                //{
                curPos = positions.Pop();
                visited[curPos.y, curPos.x] = true;
                countStrokes++;
                //}
                //else
                //{
                //    positions.Dequeue();
                //}
                Print(curPos);
                if (curPos.y + 1 < maze.Length && maze[curPos.y + 1][curPos.x] != '#' && !marked[curPos.y + 1, curPos.x])   //Проверка для перехода вверх
                {
                    positions.Push(new Position(curPos.y + 1, curPos.x, curPos.path, Tuple.Create(curPos.y + 1, curPos.x)));
                    marked[curPos.y + 1, curPos.x] = true;
                }
                if (curPos.x + 1 < maze[0].Length && maze[curPos.y][curPos.x + 1] != '#' && !marked[curPos.y, curPos.x + 1])   //Проверка для перехода вправо
                {
                    positions.Push(new Position(curPos.y, curPos.x + 1, curPos.path, Tuple.Create(curPos.y, curPos.x + 1)));
                    marked[curPos.y, curPos.x + 1] = true;
                }
                if (curPos.y - 1 >= 0 && maze[curPos.y - 1][curPos.x] != '#' && !marked[curPos.y - 1, curPos.x])   //Проверка для перехода вниз
                {
                    positions.Push(new Position(curPos.y - 1, curPos.x, curPos.path, Tuple.Create(curPos.y - 1, curPos.x)));
                    marked[curPos.y - 1, curPos.x] = true;
                }
                if (curPos.x - 1 >= 0 && maze[curPos.y][curPos.x - 1] != '#' && !marked[curPos.y, curPos.x - 1])   //Проверка для перехода влево
                {
                    positions.Push(new Position(curPos.y, curPos.x - 1, curPos.path, Tuple.Create(curPos.y, curPos.x - 1)));
                    marked[curPos.y, curPos.x - 1] = true;
                }
                Console.ReadKey();

            }
            if (maze[curPos.y][curPos.x] == 'B')
            {
                Console.WriteLine();
                Console.WriteLine("Найден B");
                Console.WriteLine();
                Console.WriteLine($"y = {curPos.y}");
                Console.WriteLine($"x = {curPos.x}");
                Console.WriteLine();
                Console.WriteLine($"Количество ходов: {countStrokes}");
                PathPrint(curPos.path);
                for (int i = 0; i < maze.Length; i++)
                {
                    for (int j = 0; j < maze[0].Length; j++)
                    {
                        marked[i, j] = false;
                    }
                }
            }
            Console.ReadKey();
        }
        Position StartPosition()
        {
            for (int i = 0; i < maze.Length; i++)
            {
                for (int j = 0; j < maze[i].Length; j++)
                {
                    if (maze[i][j] == 'A')
                    {
                        return new Position(i, j);
                    }
                }
            }
            throw new Exception("Start position not found.");
        }
        void Print(Position pos)
        {
            for (int i = 0; i < maze.Length; i++)
            {
                for (int j = 0; j < maze[i].Length; j++)
                {
                    if (i == pos.y && j == pos.x)
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write("*");
                        Console.ForegroundColor = ConsoleColor.White;

                    }
                    else if (maze[i][j] == 'A')
                    {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.Write("A");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (maze[i][j] == 'B')
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("B");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (visited[i, j] == true)
                    {
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.Write(".");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (maze[i][j] == '#')
                    {
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.Write("#");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.Write(maze[i][j]);
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine($"y = {pos.y}");
            Console.WriteLine($"x = {pos.x}");
        }
        void PathPrint(List<Tuple<int, int>> path)
        {
            Console.WriteLine("Путь:");
            Console.WriteLine();
            for (int i = 0; i < maze.Length; i++)
            {
                for (int j = 0; j < maze[i].Length; j++)
                {
                    bool contains = false;
                    foreach (var p in path)
                    {
                        if (i == p.Item1 && j == p.Item2)
                        {
                            contains = true;
                            break;
                        }
                    }
                    if (maze[i][j] == 'A')
                    {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.Write("A");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (maze[i][j] == 'B')
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("B");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (contains)
                    {
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.Write(".");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else if (maze[i][j] == '#')
                    {
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.Write("#");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.Write(maze[i][j]);
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
