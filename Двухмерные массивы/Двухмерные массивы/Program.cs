﻿using System;

namespace Двухмерные_массивы
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] matrix = new int[10 , 10];
            Random r = new Random();
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            for(int i = 1; i < 10; i++)
            {
                for (int j = 1; j < 10; j++)
                {
                    matrix[i, j] = i * j;
                    Console.Write($"{matrix[i,j],2}| ");
                }
                Console.WriteLine();
            }
        }
    }
}
