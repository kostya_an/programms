﻿using System;

namespace SkillLesson2
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Переменные и вопросы
            Console.WriteLine("Введите ваше имя");
            var Name = Console.ReadLine();
            Console.WriteLine("Введите вашу фамилию");
            var LastName = Console.ReadLine();
            Console.WriteLine("Введите ваш возвраст");
            var Age = Console.ReadLine();
            Console.WriteLine("Введите ваш год рождения");
            var yearOfBirth = Console.ReadLine();
            #endregion
            #region common
            Console.WriteLine("Имя: " + Name + ", Фамилия: " + LastName + ", Возвраст: " + Age + ", Год рождения: " + yearOfBirth + ".");
            Console.ReadKey();
            #endregion
            #region count
            Console.WriteLine("Имя: {0}, Фамилия: {1}, Возвраст: {2}, Год рождения: {3}.",
                                Name, // {0}
                                LastName, // {1}
                                Age, //{2}
                                yearOfBirth); // {3}
            Console.ReadKey();
            #endregion
            #region pattern
            string pattern = "Имя: {0}, Фамилия: {1}, Возвраст: {2}, Год рождения: {3}.";
            Console.WriteLine(pattern, //Переменная в которой заданы числа, значение которых ниже
                               Name, // {0}
                               LastName, // {1}
                               Age, //{2}
                               yearOfBirth); // {3}
            Console.ReadKey();
            #endregion
            #region NewPattern
            string NewPattern = "Имя: {0} \nФамилия: {1} \nВозвраст: {2} \nГод рождения: {3}";
            Console.WriteLine(NewPattern, //Переменная в которой заданы числа, значение которых ниже и с переносом строки
                               Name, // {0}
                               LastName, // {1}
                               Age, //{2}
                               yearOfBirth); // {3}
            Console.ReadKey();
            #endregion
            #region Var in Text
            Console.WriteLine($"Имя: {Name}, Фамилия: {LastName}, Возвраст: {Age}, Год рождения: {yearOfBirth}");
            Console.ReadKey();
            #endregion
            #region форматирование в int
            int i = 1123123;
            Console.WriteLine(i); //1123123
            Console.ReadKey();

            string iFormated = i.ToString("# ### ###"); 
            Console.WriteLine(iFormated); //1 123 123
            Console.ReadKey();

            Console.WriteLine("{0:0 000 000}", i); // 1 123 123
            Console.ReadKey();
            #endregion
            #region форматирование в float
            float f = 1.123123f;
            Console.WriteLine(i); //1.123123
            Console.ReadKey();

            string fFormated = f.ToString("#.### ###");
            Console.WriteLine(fFormated); //1.123 123
            Console.ReadKey();

            Console.WriteLine("{0:0.000 000}", f); // 1.123 123
            Console.ReadKey();
            #endregion
        }
    }
}
