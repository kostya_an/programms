﻿using System;

namespace Lesson2
{
    class Program
    {
        #region Переменные
        enum end { win, nomoney, nohealth, noend, afterwin , end}
        static end GameEnd = end.noend;
        public static string Name;
        public static string Choice;
        public static int RelaxationDays;
        public static int Days;
        public static int health;
        public static int Money;
        public static int Feed;
        public static int Joy;
        public static int hunger;
        public static int dmj;
        public static int income;
        #endregion
        #region Цвета
        //Все цвета:
        //Черный - Console.ForegroundColor = ConsoleColor.Black;
        //Тёмно-Серый - Console.ForegroundColor = ConsoleColor.DarkGray;
        //Тёмно-Синий - Console.ForegroundColor = ConsoleColor.DarkBlue;
        //Тёмно-Голубой - Console.ForegroundColor = ConsoleColor.DarkCyan;
        //Тёмно-Зелёный - Console.ForegroundColor = ConsoleColor.DarkGreen;
        //Тёмно-Пурпурный - Console.ForegroundColor = ConsoleColor.DarkMagenta;
        //Тёмно-Красный - Console.ForegroundColor = ConsoleColor.DarkRed;
        //Тёмно-Жёлтый - Console.ForegroundColor = ConsoleColor.DarkYellow;
        //Серый - Console.ForegroundColor = ConsoleColor.Gray;
        //Синий - Console.ForegroundColor = ConsoleColor.Blue;
        //Голубой - Console.ForegroundColor = ConsoleColor.Cyan;
        //Зелёный - Console.ForegroundColor = ConsoleColor.Green;
        //Пурпурный - Console.ForegroundColor = ConsoleColor.Magenta;
        //Красный - Console.ForegroundColor = ConsoleColor.Red;
        //Жёлтый - Console.ForegroundColor = ConsoleColor.Yellow;
        //Белый - Console.ForegroundColor = ConsoleColor.White;
        #endregion
        static Random r = new Random();
        static void Main()
        {
            GameEnd = end.noend;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Цель игры: Работайте, гуляйте и ешьте до того момента \nПока не кончится здоровье или деньги");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Введите ваше имя!");
            Name = Console.ReadLine();
            Console.WriteLine("                                                                                                              ");
            

            Days = 0;
            RelaxationDays = 0;
            health = 10;
            Money = 10;
            Feed = 10;
            Joy = 10;
            


            while (GameEnd == end.noend)
            {
                Days = Days + 1;
                Print(Name, Days, health, Money, Feed, Joy);
                Choice = Console.ReadLine();
                hunger = r.Next(1, 3);
                dmj = r.Next(1, 3);
                income = r.Next(1, 10);
                Choices(ref Days, ref health, ref Money, ref Feed, Choice, hunger, dmj, income, ref Joy);
                fix(ref health, ref Feed, ref Joy);
                ends(Days, health, Money);
            }
            Writelosewin();


        }
        static void DaysRelaxationDays()
        {
            Joy = Joy + 10;
            Money = Money - 10;
            while (RelaxationDays < 7)
            {
                
                Days = NextDay(Days);
                RelaxationDays = RelaxationNextDay(RelaxationDays);
                RelaxationPrint(Name, Days, health, Money, Feed, Joy);
                Choice = Console.ReadLine();
                RelaxationChoices();
                fix(ref health, ref Feed, ref Joy);
                ends(Days, health, Money);
            }
            
        }
        private static void RelaxationChoices()
        {
            if (Choice == "1")
            {
                Feed = Feed - hunger;
                Money = Money + income;
                health = health - dmj;
            }
            else if (Choice == "2")
            {
                //for(int i = 0; i < 1; i++)
                //{
                Console.WriteLine("Вы прошли 1 км");
                health = health + 2;
                Feed = Feed - 2;
                //}
            }
            else if (Choice == "3")
            {
                //for (int i = 0; i < 1; i++)
                //{
                Console.WriteLine("Вы съели 1000 ккал");
                Feed = Feed + 4;
                Money = Money - 5;
                //}
            }
            else if (Choice == "4")
            {
                Console.WriteLine("Вы и так в отдыхе");
                Days = Days - 1;
                RelaxationDays = RelaxationDays - 1;
            }
            else if (Choice == "5")
            {
                health = health + 1;
                Feed = Feed - 1;
            }
            else
            {
                Console.WriteLine("Вы потратили 400 ккал, но день ещё не закончился");
                Days = Days - 1;
                RelaxationDays = RelaxationDays - 1;
                Feed = Feed - 2;
            }
        }
        static void Start()
        {

            while (GameEnd == end.noend)
            {
                ends(Days, health, Money);
                Print(Name, Days, health, Money, Feed, Joy);
                Choice = Console.ReadLine();
                Choices(ref Days, ref health, ref Money, ref Feed, Choice, hunger, dmj, income, ref Joy);
                fix(ref health, ref Feed, ref Joy);
                
            }
            Writelosewin();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Нажмите любую кнопку для выхода...");

            Console.ReadKey();

        }
        private static void ends(int Days, int health, int Money)
        {
           if (health < 1)
            {
                GameEnd = end.nohealth;
                Writelosewin();
            }
            if (Money < 1)
            {
                GameEnd = end.nomoney;
                Writelosewin();
            } 
            if (Days == 20)
            {
                GameEnd = end.win;
            }
            if (Days % 20 == 0 && Days > 20)
            {
                GameEnd = end.afterwin;
            }
            if (GameEnd == end.afterwin)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Вы хотите продолжить?");
                string Win = Console.ReadLine();
                if (Win == "Да" || Win == "да" || Win == "ДА" || Win == "Yes" || Win == "yes" || Win == "YES")
                {
                    GameEnd = end.noend;
                }
                else if (Win == "Нет" || Win == "нет" || Win == "НЕТ" || Win == "No" || Win == "no" || Win == "NO")
                {
                    Console.WriteLine("Спасибо, до свидания");
                    Console.ReadKey();
                    GameEnd = end.end;
                }
                else
                {
                    GameEnd = end.afterwin;
                }
            }
            if (GameEnd == end.win)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"{Name}, Вы выиграли");
                Console.WriteLine("Вы хотите продолжить?");
                string Win = Console.ReadLine();
                if (Win == "Да" || Win == "да" || Win == "ДА" || Win == "Yes" || Win == "yes" || Win == "YES")
                {
                    GameEnd = end.noend;
                }
                else if (Win == "Нет" || Win == "нет" || Win == "НЕТ" || Win == "No" || Win == "no" || Win == "NO")
                {
                    Console.WriteLine("Спасибо, до свидания");
                    Console.ReadKey();
                    GameEnd = end.end;

                }
                else
                {
                    GameEnd = end.win;
                }
            }
           
        }
        private static void fix(ref int health, ref int Feed, ref int Joy)
        {
            if (Joy < 1)
            {
                Feed = Feed - 1;
            }
            if (Feed < 1)
            {
                health = health - 1;
                Feed = 0;
            }
            if (Feed > 15)
            {
                Feed = 15;
            }
            if (health > 10)
            {
                health = 10;
            }
            if (Joy > 10)
            {
                Joy = 10;
            }
            if (RelaxationDays == 8)
            {
                RelaxationDays = 0;
            }
        }
        private static void Choices(ref int Days, ref int health, ref int Money, ref int Feed, string Choice, int hunger, int dmj, int income, ref int Joy)
        {
            if (Choice == "1")
            {
                Feed = Feed - hunger;
                Money = Money + income;
                health = health - dmj;
                Joy = Joy - 1;
            }
            else if (Choice == "2")
            {
                //for(int i = 0; i < 1; i++)
                //{
                Console.WriteLine("Вы прошли 1 км");
                health = health + 2;
                Feed = Feed - 2;
                Joy = Joy + 1; 
                //}
            }
            else if (Choice == "3")
            {
                //for (int i = 0; i < 1; i++)
                //{
                
                Console.WriteLine("Вы съели 1000 ккал");
                Feed = Feed + 4;
                Money = Money - 5;
                Joy = Joy + 2;
                
                //}
            }
            else if (Choice == "4")
            {
               
                DaysRelaxationDays();

            }
            else if (Choice == "5")
            {
                Console.WriteLine("Вы не в отдыхе");
            }
            else
            {
                Console.WriteLine("Вы потратили 400 ккал, но день ещё не закончился");
                Feed =- 2;
                Joy--;
            }
        }
        private static int NextDay(int Days)
        {
            Days = Days + 1;
            return Days;
        }
        private static int RelaxationNextDay(int RelaxationDays)
        {
            RelaxationDays = RelaxationDays + 1;
            return RelaxationDays;
        }
        private static void Writelosewin()
        {
           while(GameEnd == end.nomoney || GameEnd == end.nohealth)
           {

                
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"{Name}, Вы проиграли");
                    Console.WriteLine("Вы хотите продолжить?");
                    string Win = Console.ReadLine();
                    if (Win == "Да" || Win == "да" || Win == "ДА" || Win == "Yes" || Win == "yes" || Win == "YES")
                    {
                        Main();
                    }
                    else if (Win == "Нет" || Win == "нет" || Win == "НЕТ" || Win == "No" || Win == "no" || Win == "NO")
                    {
                        Console.WriteLine("Спасибо, до свидания");
                    GameEnd = end.end;
                    Console.ReadKey();
                }
                    else
                    {
                        GameEnd = end.nomoney;
                    }

           }
        }
        private static void Print(string Name, int Days, int health, int Money, int Feed, int Joy)
        {
            Console.WriteLine("                                                                                                              ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine($"Здравствуйте {Name}!");
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine($"Начался {Days} день");
            Console.WriteLine("                                                                                                              ");
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine($"Здоровье: {health}");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine($"Деньги: {Money} $");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine($"Еда: {Feed}");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine($"Настроение: {Joy}");
            Console.WriteLine("                                                                                                            ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("1 - Пойти на работу");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("2 - Погулять");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("3 - Поесть в кафе");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("4 - Взять отпуск");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("5 - Искупаться");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Другое - Ничего не делать :)");
        }
        private static void RelaxationPrint(string Name, int Days, int health, int Money, int Feed, int Joy)
        {
            Console.WriteLine("                                                                                                              ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine($"Здравствуйте {Name}!");
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine($"Начался {Days} день");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine($"Начался {RelaxationDays} выходной");
            Console.WriteLine("                                                                                                              ");
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine($"Здоровье: {health}");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine($"Деньги: {Money} $");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine($"Еда: {Feed}");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine($"Настроение: {Joy}");
            Console.WriteLine("                                                                                                            ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("1 - Пойти на работу");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("2 - Погулять");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("3 - Поесть в кафе");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("4 - Взять отпуск");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("5 - Искупаться");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Другое - Ничего не делать :)");
        }
    }
}
