﻿using System;

namespace Calculator2._0
{
    class Program
    {
        static void Main(string[] args)
        {
            double num1 = 0;
            string sign = "+";
            double num2 = 0;
            bool does = true;
            Does does1 = new Does();
            Console.WriteLine("Вас приветствует калькулятор, нажмите любую кнопку!");
            Console.ReadKey();
            while (does)
            {
                Console.Clear();
                Console.WriteLine("Введите первое число (cancel - выйти)");
                try
                {
                    string num1String = Console.ReadLine();
                    if(num1String == "cancel")
                    {
                        does = false;
                        continue;
                    }
                    num1 = Convert.ToDouble(num1String);
                    Console.WriteLine("Введите знак действия: +, -, *, /, ^, % (cancel - выйти)");
                    sign = Console.ReadLine();
                    if (sign == "cancel")
                    {
                        does = false;
                        continue;
                    }
                    Console.WriteLine("Введите второе число (cancel - выйти)");
                    string num2String = Console.ReadLine();
                    if (num2String == "cancel")
                    {
                        does = false;
                        continue;
                    }
                    num2 = Convert.ToDouble(num2String);
                }
                catch(Exception e)
                {
                    Console.WriteLine($"Неккоректно введено число: {e}");
                    Console.ReadKey();
                    continue;
                }
                if(sign == "+")
                {
                    Console.WriteLine(does1.Plus(num1, num2));
                    Console.ReadKey();
                }
                if(sign == "-")
                {
                    if(num1 < num2)
                    {
                        Console.WriteLine("Первое число меньше второго");
                        Console.ReadKey();
                        continue;
                    }
                    Console.WriteLine(does1.Minus(num1, num2));
                    Console.ReadKey();
                }
                if (sign == "*")
                {
                    Console.WriteLine(does1.Multiplicate(num1, num2));
                    Console.ReadKey();
                }
                if (sign == "^")
                {
                    Console.WriteLine(does1.Stepen(num1, num2));
                    Console.ReadKey();
                }
                if (sign == "/")
                {
                    if(num2 == 0)
                    {
                        Console.WriteLine("На 0 делить нельзя!");
                        Console.ReadKey();
                        continue;
                    }
                    Console.WriteLine(does1.Share(num1, num2));
                    Console.ReadKey();
                }
                if (sign == "%")
                {
                    if (num2 == 0)
                    {
                        Console.WriteLine("На 0 делить нельзя!");
                        Console.ReadKey();
                        continue;
                    }
                    Console.WriteLine(does1.Remainder(num1, num2));
                    Console.ReadKey();
                }
            }
            Console.ReadKey();
        }
       
    }
    class Does
    {
        public Double Plus(double a, double b)
        {
            double c = a + b;
            return c;
        }
        public Double Minus(double a, double b)
        {
            double c = a - b;
            return c;
        }
        public Double Multiplicate(double a, double b)
        {
            double c = a * b;
            return c;
        }
        public Double Share(double a, double b)
        {
            double c = a / b;
            return c;
        }
        public Double Stepen(double a, double b)
        {
            double c = Math.Pow(a, b);
            return c;
        }
        public Double Remainder(double a, double b)
        {
            double c = a % b;
            return c;
        }
    }
}
