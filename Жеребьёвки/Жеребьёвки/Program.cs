﻿using System;
using System.Collections.Generic;

namespace Жеребьёвки
{
    class Program
    {
        public static List<string> Clubs = new List<string>();
        public static List<bool> ClubsTrue = new List<bool>();
        static void Main(string[] args)
        {
            bool isClubs = false;
            int Club = 1;
            string read;
            int Club1 = 0;
            int Club2 = 0;
            Random r = new Random();
            while (!isClubs)
            {
                Console.WriteLine($"Введите {Club} команду \nЕсли вы уже ввели все команды введите Finish");
                read = Console.ReadLine();
                if(read == "Finish" && Clubs.Count == 0)
                {
                    Console.WriteLine("Введите ещё две команды");
                    Clubs.Add(Console.ReadLine());
                    Clubs.Add(Console.ReadLine());
                }
                else if(read == "Finish" && Clubs.Count % 2 != 0)
                {
                    Console.WriteLine("Введите ещё одну команду");
                    Clubs.Add(Console.ReadLine());
                }
                if(read == "Finish" && Clubs.Count > 0 && Clubs.Count % 2 == 0)
                {
                    isClubs = true;
                    continue;
                }
                else if(read != "Finish")
                {
                    Clubs.Add(read);
                }
                Club++;
            }
            for (int i = 1; i <= Clubs.Count; i++)
            {
                ClubsTrue.Add(false);
            }
            
            for(int i = 1; i <= Clubs.Count / 2; i++)
            {
                Club1 = r.Next(0, Clubs.Count / 2);
                Club2 = r.Next(Clubs.Count / 2, Clubs.Count);
                while(ClubsTrue[Club1] == true)
                {
                    Club1 = r.Next(0, Clubs.Count / 2);
                }
                while (ClubsTrue[Club2] == true)
                {
                    Club2 = r.Next(Clubs.Count / 2, Clubs.Count);
                }
                Console.WriteLine($"{Clubs[Club1]} - {Clubs[Club2]}");
            }
            
        }
    }
}
